# Hyperpod-download



## Hyperpod

Hyperpod is designed to ease the way of accessing and feteching Azure Pod Logs with simple User Interface

## Dependencies

- [ ] [Python 3](https://www.python.org/download/releases/3.0/)
- [ ] [Azure CLI](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli)

```
Python --version
az login
```

## Environmental Variable

Add Python to PATH
Add pip to PATH
Add az to PATH

## Update

User might need to update your tool if a new update is available
Update is automatic, so no action is needed

## Feedbacks
We like to hear your feedbacks - [Provide feedback here](https://3ncidicdnkc.typeform.com/to/Gj2M2SnE)