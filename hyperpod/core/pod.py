from tkinter import *
import subprocess
import regex as re
import time
import sys
import json
from PIL import Image, ImageTk
import _thread as thread
import os
import webbrowser
from idlelib.tooltip import Hovertip

subscription_ids_content = []
resourcelist = []
topic = []

buttonLoginNext = ""

labelAccountList = ""
dropdownAccountList = ""

labelClusterList = ""
dropdownClusterList = ""

labelTopicList = ""
dropdownTopicList = ""

buttonFilter = ""
topiclisttext = ""

dropdownTopicList = ""

buttonSubmit = ""
submitHt = ""
buttonBundle = ""
bundleHt = ""
buttonFile = ""
fileHt = ""
buttonServer = ""
serverHt = ""
buttonRestartPod = ""
restartpodHt = ""

state = False

currentDirectory = os.path.dirname(os.path.realpath(__file__))
originalDirectory = currentDirectory

path = currentDirectory+"\\..\\assets\\tank.ico"

root = Tk()
root.geometry("315x320")
root.title("Hyperpod")
root.iconbitmap(path)

imageLogin = ImageTk.PhotoImage(file=currentDirectory+"\\..\\assets\\image-login.png")
imageLogout = ImageTk.PhotoImage(file=currentDirectory+"\\..\\assets\\image-logout.png")
imageDownload = ImageTk.PhotoImage(file=currentDirectory+"\\..\\assets\\image-download.png")
imageFilter = ImageTk.PhotoImage(file=currentDirectory+"\\..\\assets\\image-filter.png")
imageOpenfile = ImageTk.PhotoImage(file=currentDirectory+"\\..\\assets\\image-openfile.png")
imageOpenfolder = ImageTk.PhotoImage(file=currentDirectory+"\\..\\assets\\image-openfolder.png")
imageServeron = ImageTk.PhotoImage(file=currentDirectory+"\\..\\assets\\image-serveron.png")
imageServeroff = ImageTk.PhotoImage(file=currentDirectory+"\\..\\assets\\image-serveroff.png")
imageRestartpod = ImageTk.PhotoImage(file=currentDirectory+"\\..\\assets\\image-restartpod.png")

config_data = open(currentDirectory+"\\..\\config\\config.json")
config = json.load(config_data)

def loginFinish(state, user=True):
    global buttonLoginNext
    if(state):
        if(user):
            labelLogin.config(text="You are connected to Azure Pod")
        else:
            labelLogin.config(text="Azure Pod Connected But User Not Enabled")
        labelLogin.place(x=10, y=10)
        # Create a login button at the top right of the window
        if(buttonLoginNext!=""):
            buttonLoginNext.destroy()
        buttonLoginNext = Button(root, text="Logout", image=imageLogout, compound="right", command=lambda:[thread.start_new_thread(logout, (True,))])
        buttonLoginNext.config(width=10*7, height=20)  
        buttonLoginNext.config(image=imageLogout)         
        buttonLoginNext.config(text="Logout")
        buttonLoginNext.place(x=220, y=9)
        labeltext.config(text="Welcome to Azure Pod")
        labelLogin.place(x=10, y=10)
        labeltext.config(text="v" + config["version"])
        buttonLogin.place_forget()
        lab.pack_forget()
        labellogincheck.pack_forget()
    else:
        labeltext.config(text="Welcome to Azure Pod. v" + config["version"])
        buttonLogin.config(text="Login")
        buttonLogin.config(image=imageLogin)
        buttonLogin.config(width=10*8, height=23)
        buttonLogin.place(x=25*4.5, y=35*7)
        labellogincheck.config(text="You are not logged in to Azure")
        labelLogin.config(text="You are not logged in")

def logout(fromKey = False):
    global buttonLoginNext, labelAccountList, dropdownAccountList, labelClusterList, dropdownClusterList, labelTopicList, dropdownTopicList, topiclisttext, buttonSubmit, buttonBundle, buttonFile, buttonFilter, buttonRestartPod
    if(fromKey):
        cmd = 'az logout'
        output = subprocess.check_output(cmd, shell=True)
        print(output)
        
        if(labelLogin!=""):
            labelLogin.place_forget()
        if(buttonLoginNext!=""):
            buttonLoginNext.destroy()
        if(labelAccountList!=""):
            labelAccountList.place_forget()
        if(dropdownAccountList!=""):
            dropdownAccountList.place_forget()
        if(labelClusterList!=""):
            labelClusterList.place_forget()
        if(dropdownClusterList!=""):
            dropdownClusterList.place_forget()
        if(labelTopicList!=""):
            labelTopicList.place_forget()
        if(dropdownTopicList!=""):
            dropdownTopicList.place_forget()
        if(buttonFilter!=""):
            buttonFilter.place_forget()
        if(topiclisttext!=""):
            topiclisttext.place_forget()
        if(dropdownTopicList!=""):
            dropdownTopicList.place_forget()
        if(buttonSubmit!=""):
            buttonSubmit.place_forget()
        if(buttonBundle!=""):
            buttonBundle.place_forget()
        if(buttonFile!=""):
            buttonFile.place_forget()
        if(buttonServer!=""):
            buttonServer.place_forget()
        if(buttonRestartPod!=""):
            buttonRestartPod.place_forget()
        
    labeltext.config(text="Welcome to Azure Pod. v" + config["version"])
    buttonLogin.config(text="Login")
    buttonLogin.config(image=imageLogin)
    buttonLogin.config(width=10*8, height=23)
    buttonLogin.place(x=25*5, y=35*7)
    labellogincheck.config(text="You are not logged in to Azure")
    labelLogin.config(text="You are not logged in")
    
    lab.pack(ipadx=2,ipady=20)
    labellogincheck.pack(ipadx=2, ipady=20)
    
    
def loginMFA():
    print("MFA Enabled, going with device based authentication")
    try:
        cmd = 'az login --use-device-code'
        process = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            shell=True,
            encoding='utf-8',
            errors='replace'
        )
        
        while True:
            realtime_output = process.stdout.readline()

            if realtime_output == '' and process.poll() is not None:
                break

            if realtime_output:
                value = realtime_output.strip()
                authkeyContent = str(realtime_output.strip())
                try:
                    authkey  = re.search(r'[0-9A-Z]{9}', authkeyContent).group(0)
                    print(realtime_output.strip(), flush=True)
                    print(authkey)
                    print('Going with Device Code Authentication')
                    labeltext.config(text="Going with Device Code Authentication")
                    labeltext.config(text="Key "+authkey+" already copied. Just try Pasting it")
                    cmd = 'echo ' + authkey + '| clip'
                    webbrowser.open('https://microsoft.com/devicelogin')
                    output = subprocess.check_output(cmd, shell=True)
                    print(output)
                except:
                    print(realtime_output.strip(), flush=True)
      
    except:
        print('You are not logged in. Please try again')
        labeltext.config(text="There is an Error while logging you in")
    loginFinish(True)
    
    
def checkLogin():
    labeltext.config(text="Checking Login Status")
    cmd = 'az account list -o table'
    output = subprocess.check_output(cmd, shell=True)
    print(output)
    
    subscription_ids_content = str(output).split("\\n")[2:-1]
    subscription_ids_content = [' '.join(x.split(" ")).split() for x in subscription_ids_content]
    subscription_ids_content = [[x[0],x[2]] for x in subscription_ids_content]
    subscription_ids = [x[0] for x in subscription_ids_content]
    print(subscription_ids_content)
    if len(subscription_ids)>0:
        cmd = 'az logout'
        output = subprocess.check_output(cmd, shell=True)
        print(output)
        labelLogin.config(text="You are disconnected from Azure Pod")
        labeltext.config(text="You are not Logged in")
        buttonLogin.config(text="Login")
        global labelAccountList, dropdownAccountList, labelClusterList, dropdownClusterList, labelTopicList, dropdownTopicList, buttonSubmit, topiclisttext, buttonFilter
        labelAccountList.destroy()
        dropdownAccountList.destroy()
        labelClusterList.destroy()
        dropdownClusterList.destroy()
        labelTopicList.destroy()
        dropdownTopicList.destroy()
        buttonSubmit.destroy()
        topiclisttext.destroy()
        buttonFilter.destroy()
        return False
    else:
        return True

def login(state=True):
    if(state):
        return thread.start_new_thread(getAccountList, (True))
    else:
        print("login")
        labeltext.config(text="Logging you to Azure")
        if(checkLogin()):
            cmd = 'az login'
            try:
                output = subprocess.check_output(cmd, shell=True)
                if "Enabled" in str(output):
                    loginFinish(True)
                    return getAccountList(True)
                else:
                    loginFinish(True, False)
                print(output)
            except:
                loginMFA()
                
    
def getAccountList(login=False):
    print("getAccountlist")
    if(not(login)):
        labeltext.config(text="Getting account list")
    cmd = 'az account list -o table'
    output = subprocess.check_output(cmd, shell=True)
    print(output)
    global subscription_ids_content
    subscription_ids_content = str(output).split("\\n")[2:-1]
    subscription_ids_content = [' '.join(x.split(" ")).split() for x in subscription_ids_content]
    subscription_ids_content = [[x[0],x[2]] for x in subscription_ids_content]
    subscription_ids = [x[0] for x in subscription_ids_content]
    print(subscription_ids_content)
    if len(subscription_ids)>0:
        #AccountList
        global labelAccountList, dropdownAccountList
        labelAccountList = Label(root, text="Choose the Account List")
        labeltext.config(text="")
        labelAccountList.place(x=10, y=50)
        variable = StringVar()
        variable.set("Select") # Set the default option
        dropdownAccountList = OptionMenu(root, variable, *subscription_ids, command=setSubscriptionIdThread)
        dropdownAccountList.configure(width=41, height=1, text="Select")
        dropdownAccountList.place(x=10, y=70)
        loginFinish(True)
    else:
        loginFinish(False)
      
            
def setSubscriptionIdThread(value):
    thread.start_new_thread(setSubscriptionId, (value, ))
    
def setSubscriptionId(value):
    print(value)
    labeltext.config(text="Setting Subscription Id")
    global subscription_ids_content
    value = [x[1] for x in subscription_ids_content if x[0] == value]
    value = value[0]
    print(value)
    cmd = 'az account set -s '+value
    output = subprocess.check_output(cmd, shell=True)
    print(output)
    
    labeltext.config(text="Getting Cluster List from Pod")
    cmd = 'az aks list -o table'
    output = subprocess.check_output(cmd, shell=True)
    print(output)
    clusters = []
    resourcegroup = []
    textcontents = str(output).split('\\r\\n')
    for textcontent in textcontents[2:-1]:
        try:
            clusterlist = textcontent.split(" ")
            clusterlist = [x for x in clusterlist if x != ''][0]
            resourcegrouplist = textcontent.split(" ")
            resourcegrouplist = [x for x in resourcegrouplist if x != ''][2]
            if(clusterlist!='' and resourcegrouplist!=''):
                clusters.append(clusterlist)
                resourcegroup.append(resourcegrouplist)
        except:
            pass
            
    
    global resourcelist
    resourcelist = [[x,y] for x,y in zip(clusters,resourcegroup)]
    print(resourcelist)
    #Selecting the Cluster List
    global labelClusterList, dropdownClusterList
    labelClusterList = Label(root, text="Select the Cluster")
    labelClusterList.place(x=10, y=110)
    variable = StringVar()
    variable.set("Select") # Set the default option
    dropdownClusterList = OptionMenu(root, variable, *clusters, command=setClusterThread)
    dropdownClusterList.configure(width=41, height=1, text="Select")
    dropdownClusterList.place(x=10, y=130)
    labeltext.config(text="")

def setClusterThread(value):
    thread.start_new_thread(setCluster, (value, ))
    
def setCluster(value):
    print(value)
    labeltext.config(text="Setting Cluster in Azure Pod")
    os.chdir(originalDirectory)
    global resourcelist
    print(resourcelist)
    clustername = value
    resourcename = ''
    for x in resourcelist:
        if x[0] == value:
            resourcename = x[1]
    cmd = 'az aks get-credentials -n '+clustername+' -g '+resourcename + ' --overwrite-existing'
    output = subprocess.check_output(cmd, shell=True)
    print(output)
    
    labeltext.config(text="Giving Access to Pod")
    
    cmd = currentDirectory+'\\..\\components\\kubectl config use-context '+value
    output = subprocess.check_output(cmd, shell=True)
    print(output)
    
    labeltext.config(text="Checking for Kubelogin")
    try:
        os.chdir(currentDirectory+'\\..\\components')
        
        cmd = currentDirectory+'\\..\\components\\kubectl get pods -n hip-custom'
        process = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            shell=True,
            encoding='utf-8',
            errors='replace'
        )
        
        while True:
            realtime_output = process.stdout.readline()

            if realtime_output == '' and process.poll() is not None:
                break

            if realtime_output:
                value = realtime_output.strip()
                authkeyContent = str(realtime_output.strip())
                try:
                    authkey  = re.search(r'[0-9A-Z]{9}', authkeyContent).group(0)
                    print(realtime_output.strip(), flush=True)
                    print(authkey)
                    print('Going with Key based Authentication')
                    labeltext.config(text="Going with Key based Authentication")
                    labeltext.config(text="Key "+authkey+" already copied. Just try Pasting it")
                    cmd = 'echo ' + authkey + '| clip'
                    webbrowser.open('https://microsoft.com/devicelogin')
                    output = subprocess.check_output(cmd, shell=True)
                    print(output)
                except:
                    print(realtime_output.strip(), flush=True)
      
    except:
        print('Going for Kubelogin based Authentication')
        labeltext.config(text="Going for Kubelogin based Authentication")
        try:
            cmd = currentDirectory+'\\..\\components\\kubelogin convert-kubeconfig -l azurecli'
            output = subprocess.check_output(cmd, shell=True)
            print(output)
        except:
            pass
            
    moduleList()
        
    
def moduleList():
    labeltext.config(text="Getting Module List")
          
    cmd = currentDirectory+'\\..\\components\\kubectl get pods -n hip-custom'
    output = subprocess.check_output(cmd, shell=True)
    print(output)
    
    topicList = str(output)
    topicList = topicList.split("\\n")[1:]
    
    print(topicList)
    
    topicList = [x.split(" ")[0] for x in topicList]
    
    #Selecting the Cluster List
    global labelTopicList, dropdownTopicList, topiclisttext, buttonFilter
    if(labelTopicList!=""):
        labelTopicList.destroy()
    labelTopicList = Label(root, text="Select the Module")
    labelTopicList.place(x=10, y=170)
    variable = StringVar()
    variable.set("Select") # Set the default option
    if(topiclisttext!=""):
        topiclisttext.destroy()
    topiclisttext = Entry(root)
    topiclisttext.place(x=12, y=190, height=23, width=215)
    if(buttonFilter!=""):
        buttonFilter.destroy()
    buttonFilter = Button(root, text="Filter", image=imageFilter, compound="right", command=lambda:[thread.start_new_thread(getFilter, (topiclisttext.get(), topicList))])
    buttonFilter.config(width=10*6, height=20)
    buttonFilter.config(image=imageFilter)
    buttonFilter.place(x=235, y=185)
    if(dropdownTopicList!=""):
        dropdownTopicList.destroy()
    dropdownTopicList = OptionMenu(root, variable, *topicList, command=setTopicThread)
    dropdownTopicList.configure(width=41, height=1, text="Select")
    dropdownTopicList.place(x=10, y=215)
    labeltext.config(text="")
    
def getFilter(filtertext, topicList):
    print(filtertext)
    global dropdownTopicList
    topicListSet = []
    for x in topicList:
        try:
            topicsearch = re.search(filtertext, x).group(0)
            topicListSet.append(x)
        except:
            pass
    variable = StringVar()
    variable.set("Select")
    if(len(topicListSet)>0):
        labeltext.config(text=str(len(topicListSet)) + " topics found")
    else:
        labeltext.config(text="No topics found")
    if(dropdownTopicList!=""):
        dropdownTopicList.destroy()
    dropdownTopicList = OptionMenu(root, variable, *topicListSet, command=setTopicThread)
    dropdownTopicList.configure(width=41, height=1, text="Select")
    dropdownTopicList.place(x=10, y=215)

def setTopicThread(value):
    thread.start_new_thread(setTopic, (value, ))
    
def setTopic(value):
    print(value)
    labeltext.config(text="Setting Topic")
    global topic, buttonSubmit, submitHt
    topic = value
    # Create a button at the bottom of the window
    if(buttonSubmit!=""):
        buttonSubmit.destroy()
        submitHt = ""
    buttonSubmit = Button(root, image=imageDownload, command=lambda:[thread.start_new_thread(getdata, ())])
    buttonSubmit.config(width=10*4, height=23)
    buttonSubmit.config(image=imageDownload)
    buttonSubmit.place(x=10, y=250)
    submitHt = Hovertip(buttonSubmit,'Get Pod Logs')
    #buttonSubmit.config(state="active")
    labeltext.config(text="")
    
def getdata():
    global topic, buttonBundle, bundleHt, buttonFile, fileHt, buttonServer, serverHt, buttonRestartPod, restartpodHt
    print("click " + topic)
    labeltext.config(text="Getting Pod Log for Topic")
    try:
        os.makedirs(currentDirectory+'\\..\\pod_logs\\'+topic)
    except:
        pass
    finally:
        logfile = str(time.time()).split(".")[0]
        cmd = currentDirectory+'\\..\\components\\kubectl logs '+topic+' -n hip-custom > '+currentDirectory+'\\..\\pod_logs\\' + topic + '\\log_'+ logfile +'.txt'
        output = subprocess.check_output(cmd, shell=True)
        print(output)
        
        if(buttonBundle!=""):
            buttonBundle.destroy()
            bundleHt = ""
        buttonBundle = Button(root, image=imageOpenfile, command=lambda:[thread.start_new_thread(openFile, (topic, logfile))])
        buttonBundle.config(width=10*4, height=23)
        buttonBundle.config(image=imageOpenfile)
        buttonBundle.place(x=10*7, y=250)
        bundleHt = Hovertip(buttonBundle,'Open Recently Saved Log')
        labeltext.config(text="Pod Logs saved to file")

        if(buttonFile!=""):
            buttonFile.destroy()
            fileHt = ""
        buttonFile = Button(root, image=imageOpenfolder, command=lambda:[thread.start_new_thread(openFilePath, (topic, ))])
        buttonFile.config(width=10*4, height=23)
        buttonFile.config(image=imageOpenfolder)
        buttonFile.place(x=10*13, y=250)
        fileHt = Hovertip(buttonFile,'Open Logs Folder')
           
        if(buttonServer!=""):
            buttonServer.destroy()
            serverHt = ""
        buttonServer = Button(root, image=imageServeroff, command=lambda:[thread.start_new_thread(toggleServer, ())])
        buttonServer.config(width=10*4, height=23)
        buttonServer.config(image=imageServeroff)
        buttonServer.place(x=10*19, y=250)
        serverHt = Hovertip(buttonServer,'Start Web Based Tool')
        
        if(buttonRestartPod!=""):
            buttonRestartPod.destroy()
        buttonRestartPod = Button(root, image=imageRestartpod, command=lambda:[thread.start_new_thread(restartPod, (topic, ))])
        buttonRestartPod.config(width=10*4, height=23)
        buttonRestartPod.config(image=imageRestartpod)
        buttonRestartPod.place(x=10*25, y=250)
        restartpodHt = Hovertip(buttonRestartPod,'Restart Pod')     
        
        
def openFile(topic, logfile):
    labeltext.config(text="Opening Log file")
    cmd = 'notepad '+currentDirectory+'\\..\\pod_logs\\' + topic + '\\log_'+ logfile +'.txt'
    output = subprocess.check_output(cmd, shell=True)
    print(output)
    labeltext.config(text="")
    
def openFilePath(topic):
    pathFile = currentDirectory+'\\..\\pod_logs\\' + topic
    os.startfile(pathFile)
    
def toggleServer():
    global state
    global buttonServer
    if(state==False):
        webbrowser.open('http://127.0.0.1:5000')
        state = True
        buttonServer.config(image=imageServeron)
        try:
            cmd = 'python '+currentDirectory+'\\..\\server\\app.py'
            output = subprocess.check_output(cmd, shell=True)
            print(output)
        except:
            pass
    else:
        webbrowser.open('http://127.0.0.1:5000/close')
        state = False
        buttonServer.config(image=imageServeroff)
        
def restartPod(topicname):
    try:
        labeltext.config(text="Restarting Pod. Plese wait...")
        cmd = currentDirectory+'\\..\\components\\kubectl delete pod -n hip-custom ' +topicname
        output = subprocess.check_output(cmd, shell=True)
        print(output)
        labeltext.config(text="Pod Restarted")
        moduleList()
    except:
        labeltext.config(text="Error while restarting Pod")


#root.protocol('WM_DELETE_WINDOW', toggleServer)

# Create a label at the top left of the window
labelLogin = Label(root, text="Login")
labelLogin.place_forget()

# Create a login button at the top right of the window
buttonLogin = Button(root, text="Login", image=imageLogin, compound="right", command=lambda:[thread.start_new_thread(login, (False, ))])
buttonLogin.image = imageLogin
buttonLogin.place_forget()


#login check
img  = Image.open(currentDirectory+"\\..\\assets\\tank-mini.png")
photo=ImageTk.PhotoImage(img)
lab=Label(image=photo)
lab.pack(ipadx=2,ipady=20)

labellogincheck = Label(root, text="Checking for Login...")
labellogincheck.pack(ipadx=2, ipady=20)
        

#Selecting the Topic
#label = Label(root, text="Select the Topic")
#label.place(x=spaceX, y=spaceY)
#dropdown = OptionMenu(root, StringVar(), "Option 1", "Option 2", "Option 3", "Option 4", "Option 5")
#dropdown.configure(width=41, height=1, text="Select", state="disabled")
#dropdown.place(x=spaceX, y=spaceY+20)


labeltext = Label(root, text="v" + config["version"])
labeltext.place(x=10, y=290)

if __name__ == "__main__":
    thread.start_new_thread(getAccountList, ())
    root.mainloop()