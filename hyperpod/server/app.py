import os
from flask import Flask  
from flask import render_template, render_template_string, request, Response
import os
import signal
import subprocess
import json
import webbrowser

  
app = Flask(__name__, static_url_path="", static_folder="template", template_folder='template') #creating the Flask class object 

currentDirectory = os.path.dirname(os.path.realpath(__file__))  


@app.route('/getTopics', methods=['GET']) #decorator drfines the   
def getTopics():
    if request.method == 'GET':        
        cmd = currentDirectory+'\\..\\components\\kubectl get pods -n hip-custom'
        output = subprocess.check_output(cmd, shell=True)
        print(output)
        
        topicList = str(output)
        topicList = topicList.split("\\n")[1:]
        
        print(topicList)
        
        topicList = [x.split(" ")[0] for x in topicList]
        
        topicList = json.dumps(topicList)
    
        resp = app.make_response(topicList)
        resp.mimetype = "application/json"
        return resp
    else:
        return redirect("/", code=200)
 
@app.route('/topics/<topicname>', methods=['GET']) #decorator drfines the   
def topics(topicname):
    if request.method == 'GET':        
        cmd = currentDirectory+'\\..\\components\\kubectl logs '+topicname+' -n hip-custom'
        output = subprocess.check_output(cmd, shell=True)
        resp = app.make_response(output)
        resp.mimetype = "text/plain"
        return resp
    else:
        return redirect("/", code=200)
        
@app.route('/restart/<topicname>', methods=['GET']) #decorator drfines the   
def restart(topicname):
    if request.method == 'GET':        
        cmd = currentDirectory+'\\..\\components\\kubectl delete pod -n hip-custom ' +topicname
        output = subprocess.check_output(cmd, shell=True)
        resp = app.make_response(output)
        resp.mimetype = "text/plain"
        return resp
    else:
        return redirect("/", code=200)
 
        
@app.route('/')    
def main():
    return render_template('index.html')
    

@app.route('/close')  
def close():
    os.kill(os.getpid(), signal.SIGINT)
    
    
if __name__ == "__main__":
    app.run(debug=False)