#!/bin/bash
FILE=config/newinstall.config
if test -f "$FILE"; then
    pip install -r config/requirements.txt
    rm config/newinstall.config
fi
python config/starter.py || pip install -r config/requirements.txt && python config/starter.py