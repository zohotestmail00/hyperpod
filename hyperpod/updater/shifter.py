from tkinter import *
import subprocess
from PIL import Image, ImageTk
import _thread as thread
import requests
import os
import sys
import zipfile
import shutil
import json
import time
import webbrowser
import urllib


currentDirectory = os.path.dirname(os.path.realpath(__file__))
path = currentDirectory+"\\..\\assets\\tank.ico"

root = Tk()
root.geometry("315x320")
root.title("Hyperpod")
root.iconbitmap(path)


img  = Image.open(currentDirectory+"\\..\\assets\\tank-mini.png")
photo=ImageTk.PhotoImage(img)
lab=Label(image=photo).pack(ipadx=2,ipady=20)

labelLogin = Label(root, text="Updating Hyperpod")
labelLogin.pack(ipadx=2, ipady=20)

def MyThread1():
    time.sleep(2)
    config_data = open(currentDirectory+"\..\\config\\config.json")
    config = json.load(config_data)
    try:
        r = requests.get(config["update_url"])
        response = json.loads(r.content)
        print(response)
        thread.start_new_thread(downloadFile, (response["update_url"], ))
    except:
        cmd = 'python '+currentDirectory+"\..\\core\\pod.py"
        output = subprocess.Popen(cmd, shell=True)
        root.destroy()
    finally:
        pass


def downloadFile(url):
    filezip = url.split("/")[len(url.split("/"))-1]
    file = filezip.split(".")[0]
    
    labelLogin.config(text="Download Started...")

    req = requests.get(url, stream=True)

    chunk_size=1024

    downloadpath = currentDirectory+"\..\\temp"
    if not os.path.isdir(downloadpath):
       os.makedirs(downloadpath)
       
    with open(currentDirectory+"\..\\temp\\"+filezip,'wb') as output_file:
        for chunk in req.iter_content(chunk_size):
            if chunk:
                output_file.write(chunk)
    
    labelLogin.config(text="Download Completed!!!")
    
    
    labelLogin.config(text="Extracting Files")
    

    with zipfile.ZipFile(currentDirectory+"\..\\temp\\"+filezip,"r") as zip_ref:
        zip_ref.extractall(currentDirectory+"\..\\temp")
    
    labelLogin.config(text="Installing Update")    
    original = currentDirectory+"\..\\temp\\"+file+"\\hyperpod"
    target = currentDirectory+"\..\\"

    fileList = os.listdir(original)
    print(fileList)
    for files in fileList:
        print(files)
        try:
            shutil.move(original+"\\"+files, target)
        except:
            if os.path.isdir(target+"\\"+files):
                shutil.rmtree(target+"\\"+files, ignore_errors=True)
                while os.path.isdir(target+"\\"+files):
                    pass
                shutil.move(original+"\\"+files, target)
            else:
                os.remove(target+"\\"+files)
                shutil.move(original+"\\"+files, target)
    
    labelLogin.config(text="Starting Hyperpod")
    
    
    cmd = currentDirectory+"\..\\pod.cmd"
    output = subprocess.Popen(cmd, shell=True)
    root.destroy()

if __name__ == "__main__":
    thread.start_new_thread(MyThread1, ())
    root.mainloop()