set currentdir=%~dp0
IF EXIST "%currentdir%config\newinstall.config" (
    pip install -r %currentdir%config\requirements.txt
    del %currentdir%config\newinstall.config
	python %currentdir%config\starter.py
) ELSE (
    python %currentdir%config\starter.py || pip install -r %currentdir%config\requirements.txt && python %currentdir%config\starter.py
)