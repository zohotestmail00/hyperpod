from tkinter import *
import subprocess
from PIL import Image, ImageTk
import _thread as thread
import requests
import os
import sys
import zipfile
import shutil
import json
import time
import webbrowser
import urllib


currentDirectory = os.path.dirname(os.path.realpath(__file__))
path = currentDirectory+"\\..\\assets\\tank.ico"

root = Tk()
root.geometry("315x320")
root.title("Hyperpod")
root.iconbitmap(path)


img  = Image.open(currentDirectory+"\\..\\assets\\tank-mini.png")
photo=ImageTk.PhotoImage(img)
lab=Label(image=photo).pack(ipadx=2,ipady=20)

labelLogin = Label(root, text="Checking for Update")
labelLogin.pack(ipadx=2, ipady=20)

def MyThread1():
    time.sleep(2)
    config_data = open(currentDirectory+"\config.json")
    config = json.load(config_data)
    try:
        r = requests.get(config["update_url"])
        response = json.loads(r.content)
        print(response)
        print(config)
        if(int(response["version"].split(".")[0])> int(config["version"].split(".")[0]) or int(response["version"]).split(".")[1]> int(config["version"].split(".")[1])):
            labelLogin.config(text="New version "+response["version"]+" available. Update to Continue")
            #buttonLogin = Button(root, text="Update", command=lambda:[webbrowser.open(response["update_url"])])
            #buttonLogin = Button(root, text="Update", command=lambda:[webbrowser.open(response["update_url"])])
            buttonLogin = Button(root, text="Update", command=lambda:[thread.start_new_thread(downloadFile, ())])
            buttonLogin.config(width=8, height=1)
            buttonLogin.place(x=120, y=230)
        else:
            pass
    except:
        cmd = 'python '+currentDirectory+"\..\\core\\pod.py"
        output = subprocess.Popen(cmd, shell=True)
        root.destroy()
    finally:
        pass


def downloadFile():
    cmd = 'python '+currentDirectory+"\..\\updater\\shifter.py"
    output = subprocess.Popen(cmd, shell=True)
    root.destroy()

if __name__ == "__main__":
    thread.start_new_thread(MyThread1, ())
    root.mainloop()

